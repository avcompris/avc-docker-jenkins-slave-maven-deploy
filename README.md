# avc-docker-jenkins-slave-maven-deploy

Docker image: avcompris/jenkins-slave-maven:deploy

Usage:

	$ docker run \
		-e JENKINS_PUB_KEY="ssh-rsa AAAAB3Nz...NLsug/a7" \
		avcompris/jenkins-slave-maven:deploy

Exposed port is 22.

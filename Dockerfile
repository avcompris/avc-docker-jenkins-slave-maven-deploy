# File: avc-docker-jenkins-slave-maven-deploy/Dockerfile
#
# Use to build the image: avcompris/jenkins-slave-maven:deploy

FROM avcompris/jenkins-slave-maven:base
MAINTAINER david.andriana@avantage-compris.com

#-------------------------------------------------------------------------------
#   3. USERS
#-------------------------------------------------------------------------------

USER jenkins
WORKDIR /home/jenkins

#-------------------------------------------------------------------------------
#   4. PRE-FETCHED MAVEN DEPENDENCIES
#-------------------------------------------------------------------------------

# Fabric8

RUN ./get_maven_dependency.sh io.fabric8:docker-maven-plugin:0.31.0

RUN ./get_maven_dependency.sh org.junit.jupiter:junit-jupiter:5.6.1
RUN ./get_maven_dependency.sh org.hamcrest:hamcrest-core:1.3
RUN ./get_maven_dependency.sh org.hamcrest:hamcrest:2.2
RUN ./get_maven_dependency.sh org.apache.commons:commons-lang3:3.4
RUN ./get_maven_dependency.sh commons-io:commons-io:2.4
RUN ./get_maven_dependency.sh org.apache.commons:commons-collections4:4.0
RUN ./get_maven_dependency.sh com.google.code.findbugs:jsr305:3.0.0
RUN ./get_maven_dependency.sh com.google.guava:guava:23.0

#-------------------------------------------------------------------------------
# 8. BUILDINFO (RELY ON JENKINS: POPULATED VIA "buildinfo.sh")
#-------------------------------------------------------------------------------

COPY buildinfo /

#-------------------------------------------------------------------------------
#   9. END
#-------------------------------------------------------------------------------

USER root
WORKDIR /



